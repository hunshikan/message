﻿drop index uq_username on admin_user;

drop table if exists admin_user;

drop table if exists msg_group;

drop table if exists msg_info;

drop table if exists msg_rece;

drop index uq_msg_id_email on send_email;

drop table if exists send_email;

drop index uq_msg_id_phone on send_sms;

drop table if exists send_sms;

drop table if exists sys_info;

drop index uq_groupId_sysNo_userId on user_group_rule;

drop table if exists user_group_rule;

drop index uq_userid_sourcesys on user_info;

drop table if exists user_info;

/*==============================================================*/
/* Table: admin_user                                            */
/*==============================================================*/
create table admin_user
(
   id                   varchar(32) not null comment '编号',
   username             varchar(30) not null comment '用户名',
   password             varchar(80) not null comment '密码',
   create_time          datetime not null comment '创建时间',
   primary key (id)
);

alter table admin_user comment '管理员表';

/*==============================================================*/
/* Index: uq_username                                           */
/*==============================================================*/
create unique index uq_username on admin_user
(
   username
);

/*==============================================================*/
/* Table: msg_group                                             */
/*==============================================================*/
create table msg_group
(
   id                   varchar(32) not null comment '编号',
   sys_no               varchar(50) not null comment '来源系统',
   type                 int not null comment '类型[10系统、20个人]',
   name                 varchar(50) not null comment '名称',
   pid                  varchar(32) not null comment '父编号',
   create_time          datetime not null comment '创建时间',
   primary key (id)
);

alter table msg_group comment '消息分组表';

/*==============================================================*/
/* Table: msg_info                                              */
/*==============================================================*/
create table msg_info
(
   id                   varchar(32) not null comment '编号',
   group_id             varchar(32) not null comment '消息分组编号',
   title                varchar(255) not null comment '标题',
   content              text comment '内容',
   create_time          datetime not null comment '创建时间',
   send_user_id         varchar(32) not null comment '发送人',
   status               int not null comment '状态[10待发送、20已发送]',
   send_time            datetime not null comment '发送时间',
   primary key (id)
);

alter table msg_info comment '消息表';

/*==============================================================*/
/* Table: msg_rece                                              */
/*==============================================================*/
create table msg_rece
(
   id                   varchar(32) not null comment '编号',
   msg_id               varchar(32) not null comment '消息编号',
   rece_sys_no          varchar(50) not null comment '接收人来源系统',
   rece_user_id         varchar(32) not null comment '接收人',
   rece_time            datetime not null comment '接收时间',
   is_read              int not null comment '是否阅读',
   read_time            datetime comment '阅读时间',
   primary key (id)
);

alter table msg_rece comment '消息接收表';

/*==============================================================*/
/* Table: send_email                                            */
/*==============================================================*/
create table send_email
(
   id                   varchar(32) not null comment '编号',
   msg_id               varchar(32) not null comment '消息编号',
   email                varchar(100) not null comment '接收邮箱',
   content              text not null comment '发送内容',
   send_time            datetime not null comment '发送时间',
   status               int not null comment '状态[10待发送、20发送中、30发送成功、40发送失败]',
   create_time          datetime not null comment '创建时间',
   serv_no              varchar(32) not null comment '处理服务的唯一编码',
   files                varchar(500) comment '附件集合[多个;分隔]',
   primary key (id)
);

alter table send_email comment '发送邮件信息表';

/*==============================================================*/
/* Index: uq_msg_id_email                                       */
/*==============================================================*/
create unique index uq_msg_id_email on send_email
(
   msg_id,
   email
);

/*==============================================================*/
/* Table: send_sms                                              */
/*==============================================================*/
create table send_sms
(
   id                   varchar(32) not null comment '编号',
   msg_id               varchar(32) not null comment '消息编号',
   phone                varchar(30) not null comment '手机号',
   content              text not null comment '发送内容',
   send_time            datetime not null comment '发送时间',
   status               int not null comment '状态[10待发送、20发送中、30发送成功、40发送失败]',
   create_time          datetime not null comment '创建时间',
   serv_no              varchar(32) not null comment '处理服务的唯一编码',
   primary key (id)
);

alter table send_sms comment '发送短信信息表';

/*==============================================================*/
/* Index: uq_msg_id_phone                                       */
/*==============================================================*/
create unique index uq_msg_id_phone on send_sms
(
   msg_id,
   phone
);

/*==============================================================*/
/* Table: sys_info                                              */
/*==============================================================*/
create table sys_info
(
   sys_no               varchar(50) not null comment '系统编码',
   name                 varchar(150) not null comment '系统名称',
   create_time          datetime not null comment '创建时间',
   primary key (sys_no)
);

alter table sys_info comment '来源系统表';

/*==============================================================*/
/* Table: user_group_rule                                       */
/*==============================================================*/
create table user_group_rule
(
   id                   varchar(32) not null comment '编号',
   group_id             varchar(32) not null comment '分组编号',
   sys_no               varchar(32) not null comment '来源系统',
   user_id              varchar(32) not null comment '用户编码',
   status               int not null comment '状态[10打开、20关闭]',
   email_status         int not null comment '发邮件[10打开、20关闭]',
   sms_status           int not null comment '发短信[10打开、20关闭]',
   rece_email           varchar(200) comment '接收邮箱[多个用;分隔]',
   rece_phone           varchar(200) comment '接收手机',
   primary key (id)
);

alter table user_group_rule comment '用户消息分组规则表';

/*==============================================================*/
/* Index: uq_groupId_sysNo_userId                               */
/*==============================================================*/
create unique index uq_groupId_sysNo_userId on user_group_rule
(
   group_id,
   sys_no,
   user_id
);

/*==============================================================*/
/* Table: user_info                                             */
/*==============================================================*/
create table user_info
(
   id                   varchar(32) not null comment '编号',
   sys_no               varchar(50) not null comment '来源系统',
   user_id              varchar(32) not null comment '用户编号',
   create_time          datetime not null comment '创建时间',
   phone                varchar(255) comment '手机号[多个;分隔]',
   email                varchar(255) comment '邮箱[多个;分隔]',
   primary key (id)
);

alter table user_info comment '用户表';

/*==============================================================*/
/* Index: uq_userid_sourcesys                                   */
/*==============================================================*/
create unique index uq_userid_sourcesys on user_info
(
   sys_no,
   user_id
);
