package com.message.admin.send.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.message.admin.send.pojo.SendEmail;

/**
 * send_email的Dao
 * @author autoCode
 * @date 2017-12-13 11:15:57
 * @version V1.0.0
 */
public interface SendEmailDao {

	public abstract void save(SendEmail sendEmail);

	public abstract void update(SendEmail sendEmail);

	public abstract void delete(@Param("id")String id);

	public abstract SendEmail get(@Param("id")String id);

	public abstract List<SendEmail> findSendEmail(SendEmail sendEmail);
	
	public abstract int findSendEmailCount(SendEmail sendEmail);

	public abstract List<SendEmail> findIng(@Param("servNo")String servNo);

	public abstract void updateWaitToIng(@Param("servNo")String servNo, @Param("num")Integer num);

	public abstract void updateStatus(@Param("id")String id, @Param("status")Integer status);
}