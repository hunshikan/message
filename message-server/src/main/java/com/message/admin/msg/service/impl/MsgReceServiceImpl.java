package com.message.admin.msg.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.message.admin.msg.dao.MsgReceDao;
import com.message.admin.msg.pojo.MsgInfo;
import com.message.admin.msg.pojo.MsgRece;
import com.message.admin.msg.service.MsgReceService;
import com.message.admin.send.service.SendEmailService;
import com.message.admin.send.service.SendSmsService;
import com.message.admin.sys.enums.UserGroupRuleStatus;
import com.message.admin.sys.pojo.UserGroupRule;
import com.message.admin.sys.service.UserGroupRuleService;
import com.system.comm.enums.Boolean;
import com.system.comm.model.Page;
import com.system.comm.utils.FrameNoUtil;
import com.system.comm.utils.FrameStringUtil;
import com.system.comm.utils.FrameTimeUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * msg_rece的Service
 * @author autoCode
 * @date 2017-12-04 17:13:15
 * @version V1.0.0
 */
@Component
public class MsgReceServiceImpl implements MsgReceService {

	@Autowired
	private MsgReceDao msgReceDao;
	@Autowired
	private UserGroupRuleService userGroupRuleService;
	@Autowired
	private SendEmailService sendEmailService;
	@Autowired
	private SendSmsService sendSmsService;

	@Override
	public MsgRece get(String id) {
		return msgReceDao.get(id);
	}

	@Override
	public ResponseFrame pageQuery(MsgRece msgRece) {
		msgRece.setDefPageSize();
		ResponseFrame frame = new ResponseFrame();
		int total = msgReceDao.findMsgReceCount(msgRece);
		List<MsgRece> data = null;
		if(total > 0) {
			data = msgReceDao.findMsgRece(msgRece);
		}
		Page<MsgRece> page = new Page<MsgRece>(msgRece.getPage(), msgRece.getSize(), total, data);
		frame.setBody(page);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public ResponseFrame delete(String id) {
		ResponseFrame frame = new ResponseFrame();
		msgReceDao.delete(id);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public ResponseFrame saveList(MsgInfo msgInfo, List<String> receUserIds) {
		ResponseFrame frame = new ResponseFrame();
		for (String receUserId : receUserIds) {
			//判断接收人是否打开接收消息的设置
			UserGroupRule ugr = userGroupRuleService.getByGsu(msgInfo.getGroupId(), msgInfo.getSysNo(), receUserId);
			if(ugr != null && UserGroupRuleStatus.CLOSE.getCode() == ugr.getStatus().intValue()) {
				continue;
			}
			MsgRece msgRece = new MsgRece();
			msgRece.setId(FrameNoUtil.uuid());
			msgRece.setMsgId(msgInfo.getId());
			msgRece.setReceSysNo(msgInfo.getSysNo());
			msgRece.setReceUserId(receUserId);
			msgRece.setReceTime(FrameTimeUtil.getTime());
			msgRece.setIsRead(Boolean.FALSE.getCode());
			msgReceDao.save(msgRece);

			if(ugr == null) {
				continue;
			}
			String receContent = msgInfo.getReceContent();
			if(FrameStringUtil.isEmpty(receContent)) {
				receContent = msgInfo.getContent();
			}
			//发送短信
			if(FrameStringUtil.isNotEmpty(ugr.getRecePhone()) && UserGroupRuleStatus.OPEN.getCode() == ugr.getSmsStatus().intValue()) {
				sendSmsService.saveList(msgInfo.getId(), receContent, ugr.getRecePhone(), msgInfo.getSendTime());
			}
			//发送邮件
			if(FrameStringUtil.isNotEmpty(ugr.getReceEmail()) &&  UserGroupRuleStatus.OPEN.getCode() == ugr.getEmailStatus().intValue()) {
				sendEmailService.saveList(msgInfo.getId(), receContent, ugr.getReceEmail(), msgInfo.getSendTime(), msgInfo.getReceEmailFiles());
			}
		}
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public Integer getCountUnread(String receSysNo, String receUserId) {
		return msgReceDao.getCountUnread(receSysNo, receUserId);
	}

	@Override
	public ResponseFrame updateIsRead(String msgId, String sysNo, String userId, Integer isRead) {
		ResponseFrame frame = new ResponseFrame();
		MsgRece msgRece = getByMsgIdReceUserId(msgId, userId);
		if(msgRece == null) {
			frame.setCode(-2);
			frame.setMessage("不存在该消息");
			return frame;
		}
		if(isRead == null) {
			isRead = Boolean.TRUE.getCode();
		}
		msgReceDao.updateIsRead(msgRece.getId(), isRead);
		frame.setSucc();
		return frame;
	}

	@Override
	public MsgRece getByMsgIdReceUserId(String msgId, String receUserId) {
		return msgReceDao.getByMsgIdReceUserId(msgId, receUserId);
	}

	@Override
	public List<MsgRece> findByMsgId(String msgId) {
		return msgReceDao.findByMsgId(msgId);
	}
}