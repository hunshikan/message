package com.message.admin.sys.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.message.admin.sys.pojo.UserGroupRule;
import com.system.handle.model.ResponseFrame;

/**
 * user_group_rule的Service
 * @author autoCode
 * @date 2017-12-26 15:20:29
 * @version V1.0.0
 */
@Component
public interface UserGroupRuleService {
	
	/**
	 * 保存或修改
	 * @param userGroupRule
	 * @return
	 */
	public ResponseFrame saveOrUpdate(UserGroupRule userGroupRule);
	
	/**
	 * 根据id获取对象
	 * @param id
	 * @return
	 */
	public UserGroupRule get(String id);

	/**
	 * 分页获取对象
	 * @param userGroupRule
	 * @return
	 */
	public ResponseFrame pageQuery(UserGroupRule userGroupRule);
	
	/**
	 * 根据id删除对象
	 * @param id
	 * @return
	 */
	public ResponseFrame delete(String id);

	public List<UserGroupRule> findBySysNoUserId(String sysNo, String userId);

	UserGroupRule getByGsu(String groupId, String sysNo, String userId);
}