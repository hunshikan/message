package com.message.admin.sys.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.system.comm.model.KvEntity;

/**
 * 状态[10打开、20关闭]
 * @author yuejing
 * @date 2017年12月13日 下午5:52:03
 * @version V1.0.0
 */
public enum UserGroupRuleStatus {
	OPEN	(10, "打开"),
	CLOSE	(20, "关闭"),
	;
	
	private int code;
	private String name;
	private static List<KvEntity> list = new ArrayList<KvEntity>();
	private static Map<Integer, String> map = new HashMap<Integer, String>();

	private UserGroupRuleStatus(int code, String name) {
		this.code = code;
		this.name = name;
	}
	
	static {
		EnumSet<UserGroupRuleStatus> set = EnumSet.allOf(UserGroupRuleStatus.class);
		for(UserGroupRuleStatus e : set){
			map.put(e.getCode(), e.getName());
			list.add(new KvEntity(String.valueOf(e.getCode()), e.getName()));
		}
	}

	/**
	 * 根据Code获取对应的汉字
	 * @param code
	 * @return
	 */
	public static String getText(Integer code) {
		return map.get(code);
	}
	
	/**
	 * 获取集合
	 * @return
	 */
	public static List<KvEntity> getList() {
		return list;
	}

	public int getCode() {
		return code;
	}
	public String getName() {
		return name;
	}
}
