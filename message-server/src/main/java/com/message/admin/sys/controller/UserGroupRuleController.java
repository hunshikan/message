package com.message.admin.sys.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.message.admin.sys.pojo.UserGroupRule;
import com.message.admin.sys.service.UserGroupRuleService;
import com.monitor.api.ApiInfo;
import com.monitor.api.ApiParam;
import com.monitor.api.ApiRes;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * 设置用户分组规则的Controller
 * @author autoCode
 * @date 2017-12-26 15:20:28
 * @version V1.0.0
 */
@RestController
public class UserGroupRuleController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserGroupRuleController.class);

	@Autowired
	private UserGroupRuleService userGroupRuleService;
	
	@RequestMapping(name = "新增或修改", value = "/userGroupRule/saveOrUpdate")
	@ApiInfo(params = {
			@ApiParam(name="分组编号", code="groupId", value=""),
			@ApiParam(name="来源系统", code="sysNo", value=""),
			@ApiParam(name="用户编码", code="userId", value=""),
			@ApiParam(name="状态[10打开、20关闭]", code="status", value=""),
			@ApiParam(name="发邮件[10打开、20关闭]", code="emailStatus", value=""),
			@ApiParam(name="发短信[10打开、20关闭]", code="smsStatus", value=""),
			@ApiParam(name="接收手机[多个;分隔]", code="recePhone", value="", required=false),
			@ApiParam(name="接收邮箱[多个;分隔]", code="receEmail", value="", required=false),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
	})
	public ResponseFrame saveOrUpdate(UserGroupRule userGroupRule) {
		try {
			ResponseFrame frame = userGroupRuleService.saveOrUpdate(userGroupRule);
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}

	@RequestMapping(name = "分页查询信息", value = "/userGroupRule/find")
	@ApiInfo(params = {
			@ApiParam(name="来源系统", code="sysNo", value=""),
			@ApiParam(name="用户编码", code="userId", value=""),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
			@ApiRes(name="当前页码", code="page", pCode="body", clazz=Integer.class, value="1"),
			@ApiRes(name="每页大小", code="size", pCode="body", clazz=Integer.class, value="10"),
			@ApiRes(name="总页数", code="totalPage", pCode="body", clazz=Integer.class, value="5"),
			@ApiRes(name="总记录数", code="total", pCode="body", clazz=Integer.class, value="36"),
			@ApiRes(name="数据集合", code="rows", pCode="body", clazz=List.class, value=""),
			@ApiRes(name="编号", code="id", pCode="rows", value=""),
			@ApiRes(name="分组编号", code="groupId", pCode="rows", value=""),
			@ApiRes(name="分组名称", code="groupName", pCode="rows", value=""),
			@ApiRes(name="系统编号", code="sysNo", pCode="rows", value=""),
			@ApiRes(name="用户编号", code="userId", pCode="rows", value=""),
			@ApiRes(name="状态[10打开、20关闭]", code="status", pCode="rows", value=""),
			@ApiRes(name="状态名称", code="statusName", pCode="rows", value=""),
	})
	public ResponseFrame findBySysNoUserId(String sysNo, String userId) {
		try {
			ResponseFrame frame = new ResponseFrame();
			List<UserGroupRule> list = userGroupRuleService.findBySysNoUserId(sysNo, userId);
			frame.setBody(list);
			frame.setSucc();
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}

}