package com.message.admin.sys.pojo;

import java.io.Serializable;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * user_group_rule实体
 * @author autoCode
 * @date 2017-12-26 15:20:29
 * @version V1.0.0
 */
@Alias("userGroupRule")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class UserGroupRule extends BaseEntity implements Serializable {
	//编号
	private String id;
	//分组编号
	private String groupId;
	//来源系统
	private String sysNo;
	//用户编码
	private String userId;
	//状态[10打开、20关闭]
	private Integer status;
	//发邮件[10打开、20关闭]
	private Integer emailStatus;
	//发短信[10打开、20关闭]
	private Integer smsStatus;
	//接收手机[多个;分隔]
	private String recePhone;
	//接收邮箱[多个;分隔]
	private String receEmail;
	
	//============================ 扩展属性
	//分组名称
	private String groupName;
	//状态名称
	private String statusName;
	
	public UserGroupRule() {
		super();
	}
	public UserGroupRule(String groupId, String sysNo, String userId,
			Integer status, String groupName) {
		super();
		this.groupId = groupId;
		this.sysNo = sysNo;
		this.userId = userId;
		this.status = status;
		this.groupName = groupName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	
	public String getSysNo() {
		return sysNo;
	}
	public void setSysNo(String sysNo) {
		this.sysNo = sysNo;
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	public Integer getEmailStatus() {
		return emailStatus;
	}
	public void setEmailStatus(Integer emailStatus) {
		this.emailStatus = emailStatus;
	}
	public Integer getSmsStatus() {
		return smsStatus;
	}
	public void setSmsStatus(Integer smsStatus) {
		this.smsStatus = smsStatus;
	}
	public String getRecePhone() {
		return recePhone;
	}
	public void setRecePhone(String recePhone) {
		this.recePhone = recePhone;
	}
	public String getReceEmail() {
		return receEmail;
	}
	public void setReceEmail(String receEmail) {
		this.receEmail = receEmail;
	}
}