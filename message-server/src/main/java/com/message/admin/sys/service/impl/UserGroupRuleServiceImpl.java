package com.message.admin.sys.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.message.admin.msg.pojo.MsgGroup;
import com.message.admin.msg.service.MsgGroupService;
import com.message.admin.sys.dao.UserGroupRuleDao;
import com.message.admin.sys.enums.UserGroupRuleStatus;
import com.message.admin.sys.pojo.UserGroupRule;
import com.message.admin.sys.service.UserGroupRuleService;
import com.system.comm.model.Page;
import com.system.comm.utils.FrameNoUtil;
import com.system.handle.model.ResponseFrame;
import com.system.handle.model.ResponseCode;

/**
 * user_group_rule的Service
 * @author autoCode
 * @date 2017-12-26 15:20:29
 * @version V1.0.0
 */
@Component
public class UserGroupRuleServiceImpl implements UserGroupRuleService {

	@Autowired
	private UserGroupRuleDao userGroupRuleDao;
	@Autowired
	private MsgGroupService msgGroupService;
	
	@Override
	public ResponseFrame saveOrUpdate(UserGroupRule userGroupRule) {
		ResponseFrame frame = new ResponseFrame();
		UserGroupRule org = getByGsu(userGroupRule.getGroupId(), userGroupRule.getSysNo(), userGroupRule.getUserId());
		if(org == null) {
			userGroupRule.setId(FrameNoUtil.uuid());
			if(userGroupRule.getEmailStatus() == null) {
				userGroupRule.setEmailStatus(UserGroupRuleStatus.OPEN.getCode());
			}
			if(userGroupRule.getSmsStatus() == null) {
				userGroupRule.setSmsStatus(UserGroupRuleStatus.OPEN.getCode());
			}
			userGroupRuleDao.save(userGroupRule);
		} else {
			userGroupRule.setId(org.getId());
			userGroupRuleDao.update(userGroupRule);
		}
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public UserGroupRule getByGsu(String groupId, String sysNo, String userId) {
		return userGroupRuleDao.getByGsu(groupId, sysNo, userId);
	}

	@Override
	public UserGroupRule get(String id) {
		return userGroupRuleDao.get(id);
	}

	@Override
	public ResponseFrame pageQuery(UserGroupRule userGroupRule) {
		userGroupRule.setDefPageSize();
		ResponseFrame frame = new ResponseFrame();
		int total = userGroupRuleDao.findUserGroupRuleCount(userGroupRule);
		List<UserGroupRule> data = null;
		if(total > 0) {
			data = userGroupRuleDao.findUserGroupRule(userGroupRule);
		}
		Page<UserGroupRule> page = new Page<UserGroupRule>(userGroupRule.getPage(), userGroupRule.getSize(), total, data);
		frame.setBody(page);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}
	
	@Override
	public ResponseFrame delete(String id) {
		ResponseFrame frame = new ResponseFrame();
		userGroupRuleDao.delete(id);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public List<UserGroupRule> findBySysNoUserId(String sysNo, String userId) {
		List<UserGroupRule> list = new ArrayList<UserGroupRule>();
		List<MsgGroup> data = msgGroupService.findBySysNo(sysNo);
		for (MsgGroup mg : data) {
			UserGroupRule ugr = getByGsu(mg.getId(), sysNo, userId);
			if(ugr == null) {
				ugr = new UserGroupRule(mg.getId(), sysNo, userId, UserGroupRuleStatus.OPEN.getCode(), mg.getName());
			} else {
				ugr.setGroupName(mg.getName());
			}
			ugr.setStatusName(UserGroupRuleStatus.getText(ugr.getStatus()));
			list.add(ugr);
		}
		return list;
	}
}