<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld" %>
<div class="c-left left-menu">
	<div class="panel-group">
		<div class="panel panel-info top">
			<div class="panel-heading">
				<h4 class="panel-title text-center">
					<a href="${webroot}/adminUser/f-view/main">个人中心</a>
				</h4>
			</div>
		</div>
		<div class="panel panel-success center">
			<div class="panel-heading">
				<h4 class="panel-title text-center">
					<a data-toggle="collapse" href="#mlCollapsePrj">消息管理 <span
						class="caret" style="border-top-color: #468847;"></span></a>
				</h4>
			</div>
			<div id="mlCollapsePrj" class="panel-collapse collapse <c:if test="${param.first == 'msg'}">in</c:if>">
				<div class="panel-body">
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'sysInfoManager'}">info</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/sysInfo/f-view/manager">系统管理</a>
					</div>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'userInfoManager'}">info</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/userInfo/f-view/manager">系统用户</a>
					</div>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'msgInfoManager'}">info</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/msgInfo/f-view/manager">消息列表</a>
					</div>
					<%-- <div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'logInfoManager'}">info</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/logInfo/f-view/manager">日志管理</a>
					</div>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'cacheInfoManager'}">info</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/cacheInfo/f-view/manager.shtml">缓存管理</a>
					</div> --%>
				</div>
			</div>
		</div>
		<div class="panel panel-default bottom">
			<div class="panel-heading">
				<h4 class="panel-title text-center">
					<a data-toggle="collapse" href="#mlCollapseSys">系统管理 <span
						class="caret" style="border-top-color: #468847;"></span></a>
				</h4>
			</div>
			<div id="mlCollapseSys" class="panel-collapse collapse <c:if test="${param.first == 'sys'}">in</c:if>">
				<div class="panel-body">
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'adminUserManager'}">info</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block"
							href="${webroot}/adminUser/f-view/manager">用户管理</a>
					</div>
					<%-- <div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'sysConfigManager'}">info</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block"
							href="${webroot}/sysConfig/f-view/manager.shtml">系统配置</a>
					</div> --%>
				</div>
			</div>
		</div>
	</div>
</div>